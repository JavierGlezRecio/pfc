﻿using Microsoft.VisualBasic;
using Microsoft.VisualBasic.CompilerServices;
using Microsoft.Win32;
using System;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Management;
using System.Net;
using System.Net.Security;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Security.Cryptography.X509Certificates;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Forms;



namespace BQ_Multi_Fastboot
{
    /// <summary>
    /// Class to upgrade the firmware from Qualcomm-based BQ devices.
    /// </summary>
    public partial class FastbootForm : Form
    {

        private Button[] btnArray;
        private Button[] cnclArray;
        private ProgressBar[] prgArray;
        private Label[] lblArray;
        private Label[] snArray;
        private Label[] lctnArray;
        private Process[] procArray;
        private Thread[] trdArray;
        private int btnThread;
        private int threadsRunning;
        private string serial;
        private string[] IDsRunning;

        public FastbootForm()
        {
            Load += new EventHandler(FastbootForm_Load);
            FormClosing += new FormClosingEventHandler(this.FastbootForm_FormClosing);
            btnArray = new Button[17];
            cnclArray = new Button[17];
            prgArray = new ProgressBar[17];
            lblArray = new Label[17];
            snArray = new Label[17];
            lctnArray = new Label[17];
            procArray = new Process[17];
            trdArray = new Thread[17];
            IDsRunning = new string[17];
            serial = "";
            InitializeComponent();

        }

        /// <summary>
        /// Initializacion of all the UI buttons, bars and labels.
        /// </summary>
        private void AddButtons()
        {
            int xPos = 6;
            int xProgress = 330;
            int xLctn = 119;
            int xSN = 209;
            int xLbl = 536;
            int xCncl = 579;
            int yPos = -5;

            for (int i = 0; i <= 15; i++)
            {

                if ((i % 4) == 0)
                {
                    yPos = yPos + 20;
                }

                btnArray[i] = new Button();
                btnArray[i].Tag = i;
                btnArray[i].Width = 110;
                btnArray[i].Height = 25;

                btnArray[i].Left = xPos;
                btnArray[i].Top = yPos;
                btnArray[i].Text = "Conectar al teléfono";
                btnArray[i].Enabled = false;
                btnArray[i].Click += ClickHandler;

                cnclArray[i] = new Button();
                cnclArray[i].Tag = i;
                cnclArray[i].Width = 57;
                cnclArray[i].Height = 25;

                cnclArray[i].Left = xCncl;
                cnclArray[i].Top = yPos;
                cnclArray[i].Text = "Cancelar";
                cnclArray[i].Enabled = false;
                cnclArray[i].Click += CancelHandler;

                prgArray[i] = new ProgressBar();
                prgArray[i].Tag = i;
                prgArray[i].Width = 200;
                prgArray[i].Height = 25;
                prgArray[i].Visible = true;

                prgArray[i].Left = xProgress;
                prgArray[i].Top = yPos;

                snArray[i] = new Label();
                snArray[i].Tag = i;
                snArray[i].Width = 200;
                snArray[i].Text = "";

                snArray[i].Left = xSN;
                snArray[i].Top = yPos + 6;

                lblArray[i] = new Label();
                lblArray[i].Tag = i;
                lblArray[i].ForeColor = Color.Green;
                lblArray[i].Width = 40;
                lblArray[i].Font = new Font("Wingdings", 20);
                lblArray[i].Text = "";  //"ü"= CHECK; "û"= CROSS, "ý"= CROSS IN A BOX

                lblArray[i].Left = xLbl;
                lblArray[i].Top = yPos;

                lctnArray[i] = new Label();
                lctnArray[i].Tag = i;
                lctnArray[i].Width = 78;
                lctnArray[i].Text = "";

                lctnArray[i].Left = xLctn;
                lctnArray[i].Top = yPos + 6;

                GroupBox1.Controls.Add(btnArray[i]);
                GroupBox1.Controls.Add(cnclArray[i]);
                cnclArray[i].Hide();
                GroupBox1.Controls.Add(prgArray[i]);
                prgArray[i].Hide();
                GroupBox1.Controls.Add(lblArray[i]);
                GroupBox1.Controls.Add(snArray[i]);
                GroupBox1.Controls.Add(lctnArray[i]);

                yPos += 30;

            }

        }

        private void SetText(string text)
        {
            this.TextBoxLog.AppendText(text + Environment.NewLine);
        }

        private void ThreadTask()
        {
            int localBtnThread = 0;
            string flashbat = null;
            localBtnThread = btnThread;
            SwitchButtons(false);
            btnArray[localBtnThread].Text = "Inicializando...";
            threadsRunning += 1;

            cnclArray[localBtnThread].Show();
            cnclArray[localBtnThread].Enabled = true;
            prgArray[localBtnThread].Show();
            ModifyProgressBarColor.SetState(prgArray[localBtnThread], 1);

            GetSerial(localBtnThread);

            if (GetUSBLocation(localBtnThread) == null)
            {
                Interaction.MsgBox("El ordenador detecta más de un terminal pendiente de fastboot enchufado al equipo." + Environment.NewLine +
                    "Hay que ir enchufando los terminales y pulsando el botón correspondiente de uno en uno." + Environment.NewLine + Environment.NewLine +
                    "Si se trata de un error, espera a que acaben las actualizaciones en proceso y vuelve a intentarlo; o bien reinicia el programa." + Environment.NewLine +
                    "", MsgBoxStyle.Exclamation);
                ModifyProgressBarColor.SetState(prgArray[localBtnThread], 2);
                lblArray[localBtnThread].ForeColor = Color.Red;
                lblArray[localBtnThread].Text = "û";

                threadsRunning -= 1;
                CleanUpThread(localBtnThread);

                return;
            }

            UnlockDevice(serial, localBtnThread);

            //if (UnlockDevice(serial, localBtnThread) == null)
            //{
            //    //lblArray[localBtnThread].ForeColor = Color.Red;
            //    //lblArray[localBtnThread].Text = "û";
            //    //CleanUpThread(localBtnThread);
            //    //Exit Sub;
            //}
            //else
            //{
            //    //SetText("#### desbloqueo OK ####")
            //}

            using (StreamReader sr = new StreamReader(".temp\\flashbat"))
            {
                flashbat = sr.ReadLine();
            }
            

            procArray[localBtnThread] = new Process();
            procArray[localBtnThread].StartInfo.WorkingDirectory = Path.GetDirectoryName(TextBoxFile.Text);
            procArray[localBtnThread].StartInfo.FileName = "cmd.exe";
            procArray[localBtnThread].StartInfo.Arguments = string.Concat(" /C ", flashbat);
            procArray[localBtnThread].StartInfo.UseShellExecute = false;
            procArray[localBtnThread].StartInfo.RedirectStandardOutput = true;
            procArray[localBtnThread].StartInfo.RedirectStandardError = true;
            procArray[localBtnThread].StartInfo.CreateNoWindow = true;
            procArray[localBtnThread].Start();

            SwitchButtons(true);
            btnArray[localBtnThread].Text = "Actualizando...";

            while ((procArray[localBtnThread].HasExited == false))
            {
                string sLine = procArray[localBtnThread].StandardError.ReadLine();
                if ((!string.IsNullOrEmpty(sLine)))
                {
                    SetText("[" + localBtnThread + "] -> " + sLine);
                    //### Logging in TextBoxLog

                    if (sLine.StartsWith("OKAY") == true)
                    {
                        prgArray[localBtnThread].Increment(2);
                        //Unos 45 "OKAY"s por fastboot
                    }

                    else if (sLine.StartsWith("FAILED (remote: device is locked. Cannot flash images)") == true ||
                        sLine.StartsWith("FAILED (remote: device is locked. Cannot erase)") == true ||
                        sLine.StartsWith("FAILED (remote: Partition flashing is not allowed)") == true)
                    {
                        Thread msgThrd = new Thread(MsgTask);
                        msgThrd.Start("No se ha podido desbloquear el dispositivo con S/N: " + snArray[localBtnThread].Text + Environment.NewLine +
                                        "Comprobar que el S/N sea '1234567890ABCDEF' o '2letras + 6números'." + Environment.NewLine +
                                        "");
                        KillAllChildProc((uint)procArray[localBtnThread].Id);
                        ModifyProgressBarColor.SetState(prgArray[localBtnThread], 2);
                        lblArray[localBtnThread].ForeColor = Color.Red;
                        lblArray[localBtnThread].Text = "û";
                        break;
                    }

                    else if (sLine.StartsWith("FAILED (status read failed (Too many links))") == true)
                    {
                        Thread msgThrd = new Thread(MsgTask);
                        msgThrd.Start("Error al actualizar el dispositivo con con S/N: " + snArray[localBtnThread].Text + Environment.NewLine +
                                      "Comprobar si se ha soltado el cable USB." + Environment.NewLine +
                                      "");
                        KillAllChildProc((uint)procArray[localBtnThread].Id);
                        ModifyProgressBarColor.SetState(prgArray[localBtnThread], 2);
                        lblArray[localBtnThread].ForeColor = Color.Red;
                        lblArray[localBtnThread].Text = "û";
                        break;
                    }

                    else if (sLine.StartsWith("FAILED (remote: partition table doesn't exist)") == true)
                    {
                        Thread msgThrd = new Thread(MsgTask);
                        msgThrd.Start("Error al actualizar el dispositivo con con S/N: " + snArray[localBtnThread].Text + Environment.NewLine +
                                      "No se encuentra la partición que se quiere flashear." + Environment.NewLine +
                                      "Asegurarse de que el '.bat' seleccionado es el del dispositivo conectado." + Environment.NewLine +
                                      "");
                        KillAllChildProc((uint)procArray[localBtnThread].Id);
                        ModifyProgressBarColor.SetState(prgArray[localBtnThread], 2);
                        lblArray[localBtnThread].ForeColor = Color.Red;
                        lblArray[localBtnThread].Text = "û";
                        break;
                    }

                    else if (sLine.StartsWith("FAILED (remote: flash write failure)") == true)
                    {
                        Thread msgThrd = new Thread(MsgTask);
                        msgThrd.Start("Error desconocido en el dispositivo con con S/N: " + snArray[localBtnThread].Text + Environment.NewLine +
                                      "El dispositivo no permite escribir en la memoria (aunque el bootloader /parece/ desbloqueado)." + Environment.NewLine +
                                      "Intentar repetir el flasheo por si se tratase de un error puntual." + Environment.NewLine +
                                      "");
                        KillAllChildProc((uint)procArray[localBtnThread].Id);
                        ModifyProgressBarColor.SetState(prgArray[localBtnThread], 2);
                        lblArray[localBtnThread].ForeColor = Color.Red;
                        lblArray[localBtnThread].Text = "û";
                        break;
                    }

                    else if (sLine.StartsWith("rebooting...") == true)
                    {
                        prgArray[localBtnThread].Increment(100);
                        lblArray[localBtnThread].ForeColor = Color.Green;
                        lblArray[localBtnThread].Text = "ü";
                        //green check
                    }

                }

                Application.DoEvents();
            }

            //prgArray(localBtnThread).Increment(100) '100
            //lblArray(localBtnThread).ForeColor = Color.Green
            //lblArray(localBtnThread).Text = "ü"  'green check

            if (prgArray[localBtnThread].Value < 100)
            {
                ModifyProgressBarColor.SetState(prgArray[localBtnThread], 2);
                lblArray[localBtnThread].ForeColor = Color.Red;
                lblArray[localBtnThread].Text = "û";
            }

            threadsRunning -= 1;
            CleanUpThread(localBtnThread);

            //SwitchButtons(True)

            //btnArray(localBtnThread).Text = "Conectar al teléfono"

            //
            //If threadsRunning = 0 Then
            //    ButtonFile.Enabled = True
            //End If


        }

        private void MsgTask(object message)
        {
            Interaction.MsgBox(message, MsgBoxStyle.Exclamation);
        }

        private void QFILTask()
        {
        }

        private void QFILTask2()
        {
        }


        private object SwitchButtons(bool state)
        {
            for (int i = 0; i <= 15; i++)
            {
                if (procArray[i] == null)
                {
                    btnArray[i].Enabled = state;
                }
            }
            return true;
        }

        private void CleanUpThread(int threadNumb)
        {
            try
            {
                procArray[threadNumb].Kill();
            }
            catch (Exception) { }

            procArray[threadNumb].Close();
            procArray[threadNumb] = null;
            IDsRunning[threadNumb] = null;

            //SetText("#### acabando thread nº: " & threadNumb)
            SwitchButtons(true);

            btnArray[threadNumb].Text = "Conectar al teléfono";
            cnclArray[threadNumb].Enabled = false;

            //threadsRunning -= 1
            if (threadsRunning == 0)
            {
                ButtonFile.Enabled = true;
                ButtonClrBars.Enabled = true;
            }
        }



        public void ClickHandler(object sender, EventArgs e)
        {
            btnThread = Conversions.ToInteger(((Button)sender).Tag);

            ((Button)sender).Enabled = false;

            prgArray[btnThread].Value = 0;
            lblArray[btnThread].Text = "";
            snArray[btnThread].Text = "";
            lctnArray[btnThread].Text = "";
            ButtonFile.Enabled = false;
            ButtonClrBars.Enabled = false;

            trdArray[btnThread] = new Thread(new ThreadStart(ThreadTask));
            trdArray[btnThread].IsBackground = true;
            trdArray[btnThread].Start();
            //SetText("Button # " + btnThread);
        }

        public void CancelHandler(object sender, EventArgs e)
        {
            btnThread = Conversions.ToInteger(((Button)sender).Tag);

            if (MessageBox.Show("¿Seguro que quieres cancelar?", "BQ Multi-Fastboot", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation) == DialogResult.Yes)
            {
                ((Button)sender).Enabled = false;
                KillAllChildProc((uint)procArray[btnThread].Id);
                ModifyProgressBarColor.SetState(prgArray[btnThread], 2);
                lblArray[btnThread].ForeColor = Color.Red;
                lblArray[btnThread].Text = "ý";
                threadsRunning -= 1;
                CleanUpThread(btnThread);
                trdArray[btnThread].Abort();
            }
        }

        private static void KillAllChildProc(uint parentProcessId)
        {
            // NOTE: Process Ids are reused!
            ManagementObjectSearcher searcher = new ManagementObjectSearcher(@"SELECT * FROM Win32_Process WHERE ParentProcessId=" + parentProcessId);
            ManagementObjectCollection children = searcher.Get();
            if (children.Count > 0)
            {
                foreach (var proc in children)
                {
                    UInt32 childProcessId = (UInt32)proc["ProcessId"];
                    if ((int)childProcessId != Process.GetCurrentProcess().Id)
                    {
                        KillAllChildProc(childProcessId);

                        try
                        {
                            Process childProcess = Process.GetProcessById((int)childProcessId);
                            childProcess.Kill();
                        }
                        catch (Exception) { }
                    }
                }
            }
        }



        public bool AcceptAllCertifications(object sender, X509Certificate certification, X509Chain chain, SslPolicyErrors sslPolicyErrors)
        {
            return true;
        }

        private object UnlockDevice(string serial, int threadNumb)
        {
            object obj = null;
            Uri uri = new Uri(string.Concat("https://sat-api.bq-com.bq.com/api/plain/", serial));
            X509Store x509Store = new X509Store(StoreLocation.CurrentUser);
            x509Store.Open(OpenFlags.OpenExistingOnly);
            IList lists = x509Store.Certificates.Find(X509FindType.FindByThumbprint, "BECE606CC36489AF88821D0A9FE95EF7CA8C33EF", false);

            if (lists.Count == 0)
            {
                Interaction.MsgBox("Error del certificado de BQ." + Environment.NewLine +
                                    "Ir a 'C:\\Herramientas\\REPOSITORIO_TELEFONIA\\Aplicaciones útiles\\Certificado flashtool'." + Environment.NewLine +
                                    "", MsgBoxStyle.Exclamation, null);
                obj = false;
            }
            else
            {
                X509Certificate item = (X509Certificate)lists[0];
                HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(uri);
                httpWebRequest.ClientCertificates.Add(item);
                ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(this.AcceptAllCertifications);
                string end = "";
                int num = 0;
                while (end.Equals("") & num < 3)
                {
                    try
                    {
                        end = (new StreamReader(httpWebRequest.GetResponse().GetResponseStream())).ReadToEnd();
                    }
                    catch (WebException webException)
                    {
                        ProjectData.SetProjectError(webException);
                        ProjectData.SetProjectError(webException);
                        num++;
                        Thread.Sleep(500);
                        ProjectData.ClearProjectError();
                        ProjectData.ClearProjectError();
                    }
                }
                if (num.Equals(3))
                {
                    Interaction.MsgBox("Imposible conectar con el servidor de BQ." + Environment.NewLine +
                                        "Comprobar la conexión a internet o esperar unos minutos." + Environment.NewLine +
                                        "", MsgBoxStyle.Exclamation, null);
                    obj = false;
                }
                else
                {
                    object objectValue = RuntimeHelpers.GetObjectValue(RuntimeHelpers.GetObjectValue(this.RunFlastboot(string.Concat("oem bq-unlock ", end), threadNumb)));
                    Regex regex = new Regex("OKAY");
                    if ((new Regex("unknown command")).Match(Conversions.ToString(RuntimeHelpers.GetObjectValue(objectValue))).Success)
                    {
                        objectValue = RuntimeHelpers.GetObjectValue(RuntimeHelpers.GetObjectValue(this.RunFlastboot("oem unlock-go & fastboot erase config", threadNumb)));
                        if ((new Regex("command write failed")).Match(Conversions.ToString(RuntimeHelpers.GetObjectValue(objectValue))).Success)
                        {
                            Interaction.MsgBox("Error al desbloquear el bootloader." + Environment.NewLine + Environment.NewLine +
                                                "(Si es un Aquaris M5.5, puede que el terminal se haya reiniciado al desbloquearlo y que no sea un error. Intenta volver a conectarlo en modo fastboot y repetir el proceso.)" + Environment.NewLine +
                                                "", MsgBoxStyle.Exclamation, null);
                            obj = false;
                            return obj;
                        }
                        else if ((new Regex("oem unlock is not allowed")).Match(Conversions.ToString(RuntimeHelpers.GetObjectValue(objectValue))).Success)
                        {
                            using (WebClient myWebClient = new WebClient())
                            {
                                // Download the Web resource and save it into the current filesystem folder.
                                myWebClient.DownloadFile(string.Concat("http://bootloader-unlock-api.bq-com.bq.local/v2/api/file/", serial), string.Concat(".temp\\unlock-", serial, ".bin"));
                            }

                            objectValue = RuntimeHelpers.GetObjectValue(RuntimeHelpers.GetObjectValue(this.RunFlastboot(string.Concat("flash frp-unlock \"", AppDomain.CurrentDomain.BaseDirectory, ".temp\\unlock-", serial, ".bin\""), threadNumb)));
                            if ((new Regex("Secret key is invalid")).Match(Conversions.ToString(RuntimeHelpers.GetObjectValue(objectValue))).Success)
                            {
                                Interaction.MsgBox("Error al desbloquear el bootloader." + Environment.NewLine +
                                                    "", MsgBoxStyle.Exclamation, null);
                                obj = false;
                                return obj;
                            }

                        }
                    }

                    obj = (regex.Match(Conversions.ToString(RuntimeHelpers.GetObjectValue(objectValue))).Success ? true : false);
                }
            }

            return obj;
        }


        public object RunFlastboot(string args, int threadNumb)
        {
            string end;
            procArray[threadNumb] = new Process();
            procArray[threadNumb].StartInfo.WorkingDirectory = Path.GetDirectoryName(this.TextBoxFile.Text);
            procArray[threadNumb].StartInfo.FileName = "cmd.exe";
            procArray[threadNumb].StartInfo.Arguments = string.Concat(" /C fastboot ", args);
            procArray[threadNumb].StartInfo.UseShellExecute = false;
            procArray[threadNumb].StartInfo.RedirectStandardOutput = true;
            procArray[threadNumb].StartInfo.RedirectStandardError = true;
            procArray[threadNumb].StartInfo.CreateNoWindow = true;
            procArray[threadNumb].Start();

            //procArray(threadNumb) = New Process
            //procArray(threadNumb).StartInfo.WorkingDirectory = Path.GetDirectoryName(TextBox1.Text)
            //procArray(threadNumb).StartInfo.FileName = String.Concat(Path.GetDirectoryName(TextBox1.Text), "\fastboot.exe")
            //procArray(threadNumb).StartInfo.Arguments = String.Concat(" ", args)
            //procArray(threadNumb).StartInfo.UseShellExecute = False
            //procArray(threadNumb).StartInfo.RedirectStandardOutput = True
            //procArray(threadNumb).StartInfo.RedirectStandardError = True
            //procArray(threadNumb).StartInfo.CreateNoWindow = True
            //procArray(threadNumb).Start()
            
            using (StreamReader standardError = procArray[threadNumb].StandardError)
            {
                end = standardError.ReadToEnd();
                //SetText(string.Concat("##", Environment.NewLine, args, Environment.NewLine, "##", Environment.NewLine, end));
            }

            while (procArray[threadNumb] != null && !procArray[threadNumb].HasExited)
            {
                Application.DoEvents();
                Thread.Sleep(10);
            }
            return end;
        }

        private object GetUSBLocation(int threadNumb)
        {
            try
            {
                ManagementObjectCollection mbsList = null;
                ManagementObjectSearcher mbs = new ManagementObjectSearcher(@"SELECT * FROM Win32_USBDevice WHERE Description = 'Android Bootloader Interface'");
                mbsList = mbs.Get();

                object regkey = null;

                //Console.WriteLine("# mbsList #");
                //Console.WriteLine(mbsList.Count);
                //Console.WriteLine("# IDs #");
                //Console.WriteLine(IDsRunning.Count(x => x != null));
                //Console.WriteLine("##");

                if (mbsList.Count - IDsRunning.Count(x => x != null) > 1)
                {
                    return null;
                }

                foreach (ManagementObject mo in mbsList)
                {
                    // Comportamiento extraño si hay más de un móvil enchufado pendiente de fastboot,
                    // cosa que, de todos modos, tampoco debería pasar
                    if (Array.Exists(IDsRunning, ID => ID == mo["DeviceID"].ToString()))
                    {
                        continue;
                    }

                    regkey = Registry.GetValue("HKEY_LOCAL_MACHINE\\SYSTEM\\CurrentControlSet\\Enum\\" + mo["DeviceID"].ToString(), "LocationInformation", null);

                    lctnArray[threadNumb].Text = "USB: H" + regkey.ToString().Substring(18, 2) + "-P" + regkey.ToString().Substring(8, 2);

                    IDsRunning[threadNumb] = mo["DeviceID"].ToString();
                }

                return regkey.ToString();
            }
            catch (Exception)
            {
                //Interaction.MsgBox("No se puede obtener la localización física del puerto USB." + Environment.NewLine +
                //    "Puede ser un problema de permisos; intenta reiniciar el programa como administrador." + Environment.NewLine +
                //    "", MsgBoxStyle.Exclamation);
                lctnArray[threadNumb].Text = "USB: UNKN";
                return "UNKN";
            }

        }

        private object GetSerial(int threadNumb)
        {
            string output = Conversions.ToString(this.RunFlastboot("getvar serialno", threadNumb));

            int startSN = Strings.InStr(output, ":") + 1;
            int endSN = Strings.InStr(startSN, output, Constants.vbCrLf, CompareMethod.Text);
            string snSplit = "";
            if ((endSN - startSN) > 0)
            {
                snSplit = Strings.Mid(output, startSN, endSN - startSN);
            }
            else
            {
                return snSplit;
            }
           

            //Dim snSplit As String = Split(Split(output, "finished")(0))(1)
            //SetText("## output serial: ## " & output)
            //SetText("## output splitted: ## " & snSplit)

            string str = "[A-Z]{2}[0-9]{6}";
            this.serial = Regex.Match(output, str).ToString();
            if ((this.serial.Equals("")))
            {
                this.serial = "1234567890ABCDEF";
            }
            //SetText(serial)
            snArray[threadNumb].Text = snSplit;
            return this.serial;
        }



        private void ButtonFile_Click(object sender, EventArgs e)
        {
            OpenFileBAT.Title = "Seleccionar .bat de fastboot";
            //OpenFileDialog1.InitialDirectory = "C:\Herramientas\REPOSITORIO_TELEFONIA"
            OpenFileBAT.Filter = "Script fastboot (*.bat)|*.bat";
            OpenFileBAT.RestoreDirectory = true;
            OpenFileBAT.ShowDialog();
        }

        private void OpenFileBAT_FileOk(object sender, CancelEventArgs e)
        {
            Stream strm = default(Stream);
            strm = OpenFileBAT.OpenFile();
            TextBoxFile.Text = OpenFileBAT.FileName.ToString();

            try
            {
                if ((!File.Exists(string.Concat(Path.GetDirectoryName(TextBoxFile.Text), "\\fastboot.exe"))))
                {
                    Extract("BQ_Multi_Fastboot", Path.GetDirectoryName(TextBoxFile.Text), "Fastboot_Resources", "fastboot.exe");
                }
                if ((!File.Exists(string.Concat(Path.GetDirectoryName(TextBoxFile.Text), "\\AdbWinApi.dll"))))
                {
                    Extract("BQ_Multi_Fastboot", Path.GetDirectoryName(TextBoxFile.Text), "Fastboot_Resources", "AdbWinApi.dll");
                }
                if ((!File.Exists(string.Concat(Path.GetDirectoryName(TextBoxFile.Text), "\\AdbWinUsbApi.dll"))))
                {
                    Extract("BQ_Multi_Fastboot", Path.GetDirectoryName(TextBoxFile.Text), "Fastboot_Resources", "AdbWinUsbApi.dll");
                }
                if ((!File.Exists(string.Concat(Path.GetDirectoryName(TextBoxFile.Text), "\\adb.exe"))))
                {
                    Extract("BQ_Multi_Fastboot", Path.GetDirectoryName(TextBoxFile.Text), "Fastboot_Resources", "adb.exe");
                }

            }
            catch (Exception)
            {
                Interaction.MsgBox("Problema al crear los programas de fastboot en la carpeta del firmware." + Environment.NewLine +
                    "Asegurarse de que se tienen permisos de escritura en esa carpeta" + Environment.NewLine +
                    "", MsgBoxStyle.Exclamation);
                return;
            }
            

            if ((!Directory.Exists(".temp")))
            {
                Directory.CreateDirectory(".temp");
                File.SetAttributes(".temp", FileAttributes.Hidden);
            }
            if ((File.Exists(".temp\\flashbat")))
            {
                File.Delete(".temp\\flashbat");
            }

            try
            {
                string line = null;
                using (StreamWriter sw = new StreamWriter(".temp\\flashbat", false))
                {
                    using (StreamReader sr = new StreamReader(TextBoxFile.Text))
                    {
                        line = sr.ReadLine();
                        while ((line != null))
                        {
                            if (line.StartsWith("fastboot", StringComparison.OrdinalIgnoreCase) && !line.EndsWith("reboot"))
                            {
                                sw.Write(line + " & ");
                            }
                            else
                            {
                                //do nothing
                                //sw.Write(line + " & ");
                            }

                            line = sr.ReadLine();
                        }
                    }
                    sw.Write("fastboot erase config & fastboot oem full-lock & fastboot oem lock & fastboot reboot");
                    File.SetAttributes(".temp\\flashbat", FileAttributes.Hidden);
                }
            }
            catch (Exception)
            {
                Interaction.MsgBox("Problema al crear el archivo de actualización." + Environment.NewLine + "", MsgBoxStyle.Exclamation);
                return;
            }
           
            for (int i = 0; i <= 15; i++)
            {
                btnArray[i].Enabled = true;
                btnArray[i].Text = "Conectar al teléfono";
            }

        }

        private static void Extract(string nameSpace, string outDirectory, string internalFilePath, string resourceName)
        {
            //nameSpace = the namespace of your project, located right above your class' name;
            //outDirectory = where the file will be extracted to;
            //internalFilePath = the name of the folder inside visual studio which the files are in;
            //resourceName = the name of the file;
            Assembly assembly = Assembly.GetCallingAssembly();

            using (Stream s = assembly.GetManifestResourceStream(nameSpace + "." + (internalFilePath == "" ? "" : internalFilePath + ".") + resourceName))
            {
                using (BinaryReader r = new BinaryReader(s))
                {
                    using (FileStream fs = new FileStream(outDirectory + "\\" + resourceName, FileMode.OpenOrCreate))
                    {
                        using (BinaryWriter w = new BinaryWriter(fs))
                        {
                            w.Write(r.ReadBytes((int)s.Length));
                        }
                    }
                }
            }         
        }



        //private void ButtonMaincode_Click(object sender, EventArgs e)
        //{
        //    OpenFileDialog2.Title = "Seleccionar Maincode de QFIL";
        //    //OpenFileDialog1.InitialDirectory = "C:\Herramientas\REPOSITORIO_TELEFONIA"
        //    OpenFileDialog2.Filter = "Maincode (*.mbn)|*firehose*.mbn";
        //    OpenFileDialog2.RestoreDirectory = true;
        //    OpenFileDialog2.ShowDialog();
        //}

        //private void OpenFileDialog2_FileOk(object sender, CancelEventArgs e)
        //{
        //    Stream strm = default(Stream);
        //    strm = OpenFileDialog2.OpenFile();
        //    TextBoxMaincode.Text = OpenFileDialog2.FileName.ToString();
        //}



        //private void ButtonQCN_Click(object sender, EventArgs e)
        //{
        //    OpenFileDialog3.Title = "Seleccionar QCN";
        //    //OpenFileDialog1.InitialDirectory = "C:\Herramientas\REPOSITORIO_TELEFONIA"
        //    OpenFileDialog3.Filter = "Archivo QCN (*.qcn)|*.qcn";
        //    OpenFileDialog3.RestoreDirectory = true;
        //    OpenFileDialog3.ShowDialog();
        //}

        //private void OpenFileDialog3_FileOk(object sender, CancelEventArgs e)
        //{
        //    Stream strm = default(Stream);
        //    strm = OpenFileDialog3.OpenFile();
        //    TextBoxQCN.Text = OpenFileDialog3.FileName.ToString();
        //}



        private void ButtonShow_Hide_Click(object sender, EventArgs e)
        {
            if (Operators.CompareString(ButtonShow_Hide.Text, ">>", false) == 0)
            {
                Width = 924;
                ButtonShow_Hide.Text = "<<";
                return;
            }
            Width = 681;
            ButtonShow_Hide.Text = ">>";
        }

        private void ButtonClrLog_Click(object sender, EventArgs e)
        {
            TextBoxLog.Clear();
        }

        private void ButtonClrBars_Click(object sender, EventArgs e)
        {
            for (int i = 0; i <= 15; i++)
            {
                prgArray[i].Value = 0;
                prgArray[i].Hide();
                cnclArray[i].Hide();
                lblArray[i].Text = "";
                snArray[i].Text = "";
                lctnArray[i].Text = "";
            }
        }



        private void FastbootForm_Load(object sender, EventArgs e)
        {
            CheckForIllegalCrossThreadCalls = false;
            Width = 681;
            AddButtons();
            threadsRunning = 0;
        }

        private void FastbootForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (threadsRunning > 0)
            {
                if (MessageBox.Show("Hay al menos una actualización en proceso. ¿Seguro que quieres salir?", "BQ Multi-Fastboot", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation) == DialogResult.Yes)
                {
                    //try
                    //{
                    //    KillAllChildProc((uint)Process.GetCurrentProcess().Id);
                    //}
                    //catch (Exception) { }

                    for (int i = 0; i <= 15; i++)
                    {
                        if (procArray[i] != null)
                        {
                            KillAllChildProc((uint)procArray[i].Id);
                            procArray[i].Kill();
                            procArray[i].Close();
                            procArray[i] = null;
                            trdArray[i].Abort();
                        }
                    }
                }
                else
                {
                    e.Cancel = true;
                    return;
                }
            }
            if ((Directory.Exists(".temp")))
            {
                Directory.Delete(".temp", true);
            }

        }

    }



    public static class ModifyProgressBarColor
    {
        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = false)]
        static extern IntPtr SendMessage(IntPtr hWnd, uint Msg, IntPtr w, IntPtr l);
        public static void SetState(this ProgressBar pBar, int state)
        {
            SendMessage(pBar.Handle, 1040, (IntPtr)state, IntPtr.Zero);
        }
    }

}
