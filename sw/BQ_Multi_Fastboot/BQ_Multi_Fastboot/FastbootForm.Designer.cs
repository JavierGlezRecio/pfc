﻿namespace BQ_Multi_Fastboot
{
    partial class FastbootForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FastbootForm));
            this.ButtonClrBars = new System.Windows.Forms.Button();
            this.ButtonShow_Hide = new System.Windows.Forms.Button();
            this.ButtonClrLog = new System.Windows.Forms.Button();
            this.TextBoxLog = new System.Windows.Forms.TextBox();
            this.GroupBox1 = new System.Windows.Forms.GroupBox();
            this.ButtonFile = new System.Windows.Forms.Button();
            this.OpenFileBAT = new System.Windows.Forms.OpenFileDialog();
            this.TextBoxFile = new System.Windows.Forms.TextBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // ButtonClrBars
            // 
            this.ButtonClrBars.Location = new System.Drawing.Point(664, 13);
            this.ButtonClrBars.Name = "ButtonClrBars";
            this.ButtonClrBars.Size = new System.Drawing.Size(75, 75);
            this.ButtonClrBars.TabIndex = 36;
            this.ButtonClrBars.Text = "Limpiar barras";
            this.ButtonClrBars.UseVisualStyleBackColor = true;
            this.ButtonClrBars.Click += new System.EventHandler(this.ButtonClrBars_Click);
            // 
            // ButtonShow_Hide
            // 
            this.ButtonShow_Hide.Location = new System.Drawing.Point(625, 12);
            this.ButtonShow_Hide.Name = "ButtonShow_Hide";
            this.ButtonShow_Hide.Size = new System.Drawing.Size(28, 20);
            this.ButtonShow_Hide.TabIndex = 34;
            this.ButtonShow_Hide.Text = ">>";
            this.ButtonShow_Hide.UseVisualStyleBackColor = true;
            this.ButtonShow_Hide.Click += new System.EventHandler(this.ButtonShow_Hide_Click);
            // 
            // ButtonClrLog
            // 
            this.ButtonClrLog.Location = new System.Drawing.Point(745, 13);
            this.ButtonClrLog.Name = "ButtonClrLog";
            this.ButtonClrLog.Size = new System.Drawing.Size(75, 75);
            this.ButtonClrLog.TabIndex = 33;
            this.ButtonClrLog.Text = "Limpiar\r\nlog";
            this.ButtonClrLog.UseVisualStyleBackColor = true;
            this.ButtonClrLog.Click += new System.EventHandler(this.ButtonClrLog_Click);
            // 
            // TextBoxLog
            // 
            this.TextBoxLog.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.TextBoxLog.BackColor = System.Drawing.SystemColors.Info;
            this.TextBoxLog.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TextBoxLog.Location = new System.Drawing.Point(664, 94);
            this.TextBoxLog.Multiline = true;
            this.TextBoxLog.Name = "TextBoxLog";
            this.TextBoxLog.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.TextBoxLog.Size = new System.Drawing.Size(237, 505);
            this.TextBoxLog.TabIndex = 32;
            // 
            // GroupBox1
            // 
            this.GroupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.GroupBox1.Location = new System.Drawing.Point(12, 38);
            this.GroupBox1.Name = "GroupBox1";
            this.GroupBox1.Size = new System.Drawing.Size(641, 561);
            this.GroupBox1.TabIndex = 31;
            this.GroupBox1.TabStop = false;
            // 
            // ButtonFile
            // 
            this.ButtonFile.Location = new System.Drawing.Point(526, 12);
            this.ButtonFile.Name = "ButtonFile";
            this.ButtonFile.Size = new System.Drawing.Size(93, 20);
            this.ButtonFile.TabIndex = 30;
            this.ButtonFile.Text = "Seleccionar .bat";
            this.ButtonFile.UseVisualStyleBackColor = true;
            this.ButtonFile.Click += new System.EventHandler(this.ButtonFile_Click);
            // 
            // OpenFileBAT
            // 
            this.OpenFileBAT.FileOk += new System.ComponentModel.CancelEventHandler(this.OpenFileBAT_FileOk);
            // 
            // TextBoxFile
            // 
            this.TextBoxFile.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.RecentlyUsedList;
            this.TextBoxFile.Location = new System.Drawing.Point(12, 12);
            this.TextBoxFile.Name = "TextBoxFile";
            this.TextBoxFile.ReadOnly = true;
            this.TextBoxFile.Size = new System.Drawing.Size(508, 20);
            this.TextBoxFile.TabIndex = 29;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::BQ_Multi_Fastboot.Properties.Resources.BQ_logo_simbolo;
            this.pictureBox1.Location = new System.Drawing.Point(826, 13);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(75, 75);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 49;
            this.pictureBox1.TabStop = false;
            // 
            // FastbootForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(908, 606);
            this.Controls.Add(this.TextBoxLog);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.ButtonClrBars);
            this.Controls.Add(this.ButtonShow_Hide);
            this.Controls.Add(this.ButtonClrLog);
            this.Controls.Add(this.GroupBox1);
            this.Controls.Add(this.ButtonFile);
            this.Controls.Add(this.TextBoxFile);
            this.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "FastbootForm";
            this.Text = "BQ Multi FastBoot v0.13 --SOLO LABORATORIO--";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        internal System.Windows.Forms.Button ButtonClrBars;
        internal System.Windows.Forms.Button ButtonShow_Hide;
        internal System.Windows.Forms.Button ButtonClrLog;
        internal System.Windows.Forms.TextBox TextBoxLog;
        internal System.Windows.Forms.GroupBox GroupBox1;
        internal System.Windows.Forms.Button ButtonFile;
        internal System.Windows.Forms.OpenFileDialog OpenFileBAT;
        internal System.Windows.Forms.TextBox TextBoxFile;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}

